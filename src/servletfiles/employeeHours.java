package servletfiles;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "employeeHours", urlPatterns = {"/employeeHours"})
public class employeeHours extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<html><head></head><body>");

        String a = request.getParameter("johnFridayHours");
        String b = request.getParameter("ralphFridayHours");

        out.println("<h1>Friday Hours for John and Ralph</h1>");
        out.println("<p>John Total Friday Hours: " + a + "</p>");
        out.println("<p>Ralph Total Friday Hours: " + b + "</p>");
        out.println("</body></html>");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


    }
}
